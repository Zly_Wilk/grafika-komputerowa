package zadanie1;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Zadanie4 {

	public static void main(String[] args)	 {
		

		String plik0 = "plik2in";
		String plik1 = "plik4in";
		int color0 = int2RGB( 0, 0, 0 );
		int color1 = int2RGB( 255, 255, 255 );
		

		try {
			BufferedImage image0 = ImageIO.read(new File(plik0));
			BufferedImage image1 = ImageIO.read(new File(plik1));
			
			BufferedImage patternBitmap = generattePatern(image0.getWidth(), image0.getHeight(), color0, color1);
			BufferedImage patternBitmapScaled = generattePaternScaled(image0.getWidth(), image0.getHeight(), color0, color1);
			
			
			int x_res = image0.getWidth();
			int y_res = image0.getHeight();
			
			BufferedImage finalImage = new BufferedImage(x_res, y_res, BufferedImage.TYPE_INT_RGB);
			
			double factor;
			int finalColor;
			
			for (int i = 0; i < y_res; i++) {
				for (int j = 0; j < x_res; j++) {
			        color0 = image0.getRGB(j, i);
	                color1 = image1.getRGB(j, i);
	                factor = normalizeRGB(patternBitmap.getRGB(j, i));

	                //if(normalizeRGB(patternBitmap.getRGB(j, i))==0) finalColor = color0;
	                //else finalColor = color1;
	                finalColor = (int) (color0 * factor + color1 * (1 - factor));
	                finalImage.setRGB(j, i, finalColor);
	            }
	        }

			ImageIO.write( finalImage, "bmp", new File( "plik4a") );
			
			System.out.println( "Net image created successfully");
			
			finalImage = new BufferedImage(x_res, y_res, BufferedImage.TYPE_INT_RGB);
			
			for (int i = 0; i < y_res; i++) {
				for (int j = 0; j < x_res; j++) {
			        color0 = image0.getRGB(j, i);
	                color1 = image1.getRGB(j, i);
	                factor = normalizeRGB(patternBitmapScaled.getRGB(j, i));

	                //if(normalizeRGB(patternBitmap.getRGB(j, i))==0) finalColor = color0;
	                //else finalColor = color1;
	                finalColor = (int) (color0 * factor + color1 * (1 - factor));
	                finalImage.setRGB(j, i, finalColor);
	            }
	        }

			ImageIO.write( finalImage, "bmp", new File( "plik4b") );
			
			System.out.println( "Net image created successfully");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		//BufferedImage image = new BufferedImage( x_res, y_res, BufferedImage.TYPE_INT_RGB);
	}
	static int int2RGB( int red, int green, int blue){
		// 	Make sure that color intensities are in 0..255 range
		red = red & 0x000000FF;
		green = green & 0x000000FF;
		blue = blue & 0x000000FF;

		// Assemble packed RGB using bit shift operations
		return (red << 16) + (green << 8) + blue;
	}
	
public static BufferedImage generattePatern(int a, int b, int c, int d) {
		
		BufferedImage image = new BufferedImage( a, b, BufferedImage.TYPE_INT_RGB);

		for (int i = 0; i < b; i++) {
			for (int j = 0; j < a; j++) {
				image.setRGB( i, j, c );
			}
		}

        int circleRadius =10;
        int ticksX = a / (2 * circleRadius);
          int ticksY = b / (2 * circleRadius);
           
           for (int i = 0; i <=ticksY; i++) {
				for (int j = 0; j <= ticksX; j++) {
			         int posX = (j * a)/ ticksX  + circleRadius;
                   int posY = (i * b) / ticksY + circleRadius;

                   drawConcentricCircle(image, posX, posY, 10, 2, c, d);
               }
           }
		return image;
}

private static void drawConcentricCircle(BufferedImage image,int centerX,int centerY,int radius, int ringWidth,int primaryColor,int secondaryColor) {



for(int i = centerY-radius; i<centerY+radius; i++) {	
	 for(int j = centerX-radius; j<centerX+radius; j++) {	
     if (i>=0 && i < image.getHeight()  && j>=0 && j< image.getWidth()) {
         //if (Math.sqrt( ( (centerX -j) * (centerX - j) + (centerY - i) * (centerY - i) ))  <= radius) {
          int ringIndex = ((int) Math.sqrt( ( (centerX -j) * (centerX - j) + (centerY - i) * (centerY - i) )) / ringWidth);

          if (ringIndex % 2 == 0) {
              image.setRGB(j, i, secondaryColor);
          }else {
              image.setRGB(j, i, primaryColor);
          }
     // }
  }
}
}
}

private static double normalizeRGB(int rgb){
    int blue = rgb & 0xff;
    int green = (rgb & 0xff00) >> 8;
    int red = (rgb & 0xff0000) >> 16;

    return (blue + green + red) / (3 * 255.0);
}

public static BufferedImage generattePaternScaled(int a, int g, int c, int f) {
	
		int b = 10;
		 
	 int greyTab[] = new int[b];
	 int scale = (int) 255/b;
	 for(int k = 1; k<=b; k++) {
		 int grey = scale*k;
		 greyTab[k-1] = int2RGB(grey, grey, grey);
	 }
	 
	BufferedImage image = new BufferedImage( a, g,
			 BufferedImage.TYPE_INT_RGB);

			 // Create packed RGB representation of black and white colors
			 int black = int2RGB( 0, 0, 0 );
			 int white = int2RGB( 255, 255, 255 );

			 // Find coordinates of the image center
			 int x_c = a / 2;
			 int y_c = g / 2;
int i,j;
			 // Process the image, pixel by pixel
			 for ( i = 0; i <g; i++)
			 for ( j = 0; j < a; j++)
			 {
			 double d;
			 int r;
			 int l;
			 // Calculate distance to the image center
			 d = Math.sqrt( (i-y_c)*(i-y_c) + (j-x_c)*(j-x_c) );
			 final int w = 20;
			 // Find the ring index
			 r = (int)d / w;
			 l = ((int)d)%w;
			 // Make decision on the pixel color
			 // based on the ring index
			 if ( r % 2 == 0)
			 // Even ring - set black color
			 image.setRGB( j, i, black );
			 else {
			 // Odd ring - set white color
			 if(l<greyTab.length) {
				 image.setRGB( j, i, greyTab[l]);
			 }else if(l>w-b) {
					 image.setRGB( j, i, greyTab[2*b-l]);
			 }else {
				 image.setRGB( j, i, white );
			 }
			 }
			 }
			 
	return image;
}
}
