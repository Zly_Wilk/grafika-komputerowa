package zadanie1;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Zadanie1b {

	public static void main(String[] args)	 {
		
		args = new String[9];
		args[0] ="500";
		args[1] ="500";
		args[2] ="plik1b";

		int x_res = Integer.parseInt( args[0].trim() );
		int y_res = Integer.parseInt( args[1].trim() );

		BufferedImage image = new BufferedImage( x_res, y_res, BufferedImage.TYPE_INT_RGB);

		int color0 = int2RGB( 204, 0, 204 );
		int color1 = int2RGB( 102, 255, 51 );
		
		int width = 5;
		int distanceX = 20;
		int distanceY = 20;
		
		boolean startX = false;
		boolean startY = false;
		int counterX = 0;
		int counterY = 0;
		
		for (int i = 0; i < y_res; i++) {
			for (int j = 0; j < x_res; j++) {
				image.setRGB( i, j, color0 );
			}
		}
		
		for (int i = 0; i < x_res; i++) {
			
			if(i%distanceX == 0) {
					counterX = width;
					startX = true;
				}
			
			counterX--;
			if(counterX == 0)  startX = false;
				
			for (int j = 0; j < y_res; j++) {
				if(j%distanceY == 0) {
					counterY = width;
					startY = true;
				}
				
				if(startX) {
					image.setRGB( i, j, color1 );
				}
				
				if(startY) {
					image.setRGB( i, j, color1 );
				}
				
				
				counterY--;
				
				if(counterY == 0)  startY = false;
			}
		}
		try {
			ImageIO.write( image, "bmp", new File( args[2]) );
			
			System.out.println( "Net image created successfully");
		 }
		catch (IOException e){
			System.out.println( "The image cannot be stored" );
		 }
	}
	
	static int int2RGB( int red, int green, int blue){
		// 	Make sure that color intensities are in 0..255 range
		red = red & 0x000000FF;
		green = green & 0x000000FF;
		blue = blue & 0x000000FF;

		// Assemble packed RGB using bit shift operations
		return (red << 16) + (green << 8) + blue;
	}
}
