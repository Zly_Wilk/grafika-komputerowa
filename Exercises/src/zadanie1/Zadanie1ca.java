package zadanie1;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Zadanie1ca {

	public static void main(String[] args)	 {
		
		args = new String[4];
		args[0] = "500";
		args[1] =	"500";
		args[2] ="plik1ca";
		
		int x_res = Integer.parseInt( args[0].trim() );
		int y_res = Integer.parseInt( args[1].trim() );

		BufferedImage image = new BufferedImage( x_res, y_res, BufferedImage.TYPE_INT_RGB);

		int color0 = int2RGB( 204, 0, 204 );
		int color1 = int2RGB( 102, 255, 51 );
		int squareSize = 50;
		  int squaresX = x_res / squareSize;
			  int squaresY = y_res / squareSize;
			        
		 boolean wasBlack = true;

			for(int i=0; i<squaresY; i++) {       
			            wasBlack = !wasBlack;
			            for(int j=0; j<squaresX; j++) {        

			                int y = i * squareSize;
			                int x = j * squareSize;

			                for(int ii=y; ii<y+squareSize; ii++) {      
			                	for(int jj=x; jj<x+squareSize; jj++) {   

			                        if (wasBlack) {
			                            image.setRGB(jj, ii, color0);
			                        } else {
			                            image.setRGB(jj, ii, color1);
			                        }
			                    }
			                }

			                wasBlack = !wasBlack;
			            }
			        }
		 
		try {
			ImageIO.write( image, "bmp", new File( args[2]) );
			
			System.out.println( "Chessboard image created successfully");
		 }
		catch (IOException e){
			System.out.println( "The image cannot be stored" );
		 }
	}

	static int int2RGB( int red, int green, int blue)
	{
	// Make sure that color intensities are in 0..255 range
	red = red & 0x000000FF;
	green = green & 0x000000FF;
	blue = blue & 0x000000FF;

	// Assemble packed RGB using bit shift operations
	return (red << 16) + (green << 8) + blue;
	}
}
