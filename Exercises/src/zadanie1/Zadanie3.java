package zadanie1;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Zadanie3 {

	public static void main(String[] args)	 {
		
		args = new String[9];
		args[0] ="500";
		args[1] ="500";
		args[2] ="plik3a";

		int x_res = Integer.parseInt( args[0].trim() );
		int y_res = Integer.parseInt( args[1].trim() );
		
		BufferedImage image = new BufferedImage( x_res, y_res, BufferedImage.TYPE_INT_RGB);

		int color0 = int2RGB( 204, 0, 204 );
		int color1 = int2RGB( 102, 255, 51 );
		
		int circleRadius = 10;
		int circleDistance = 10;
		
		for (int i = 0; i < y_res; i++) {
			for (int j = 0; j < x_res; j++) {
				image.setRGB( i, j, color0 );
			}
		}
		
		int centerDistance = 2 * circleRadius + circleDistance;

		        int ticksX = x_res / centerDistance;
		        int ticksY = y_res / centerDistance;

		        for (int i = 0; i <=ticksY; i++) {
					for (int j = 0; j <= ticksX; j++) {
						int posX = (j  * x_res)/ ticksX;
				                int posY = (i* y_res )/ ticksY ;

				                drawCircle(image, posX, posY, circleRadius, color1);
					}
				}
		        
		        try {
					ImageIO.write( image, "bmp", new File( args[2]) );
					
					System.out.println( "Rings image created successfully");
				 }
				catch (IOException e){
					System.out.println( "The image cannot be stored" );
				 }
		        
		        
		        args[2] ="plik3d";    

		        image = new BufferedImage( x_res, y_res, BufferedImage.TYPE_INT_RGB);
		        
		        for (int i = 0; i < y_res; i++) {
					for (int j = 0; j < x_res; j++) {
						image.setRGB( i, j, color0 );
					}
				}

		        
		        ticksX = x_res / (2 * circleRadius);
		           ticksY = y_res / (2 * circleRadius);
		           
		           for (int i = 0; i <=ticksY; i++) {
						for (int j = 0; j <= ticksX; j++) {
					         int posX = (j * x_res)/ ticksX  + circleRadius;
		                   int posY = (i * y_res) / ticksY + circleRadius;

		                   drawConcentricCircle(image, posX, posY, circleRadius, circleRadius/5, color0, color1);
		               }
		           }

		           try {
						ImageIO.write( image, "bmp", new File( args[2]) );
						
						System.out.println( "Rings image created successfully");
					 }
					catch (IOException e){
						System.out.println( "The image cannot be stored" );
					 }
		           
		           args[2] ="plik3e";    

			        image = new BufferedImage( x_res, y_res, BufferedImage.TYPE_INT_RGB);
			        
			        int xCenter = x_res / 2;
			                int yCenter = y_res / 2;

			                int xShifted;
			                int yShifted;
			                double fi;
			                int degree = 10;
			                for (int i = 0; i < y_res; i++) {
			        			for (int j = 0; j < x_res; j++) {
			        		       xShifted = j - xCenter;
			                        yShifted = i - yCenter;

			                        fi = Math.toDegrees(Math.atan2(yShifted, xShifted));

			                        if (fi < 0) {
			                            if (((int)fi / degree) % 2 == 0) {
			                                image.setRGB(j, i, color0);
			                            } else {
			                                image.setRGB(j, i, color1);
			                            }
			                        } else {
			                            if (((int)fi / degree) % 2 == 1) {
			                                image.setRGB(j, i, color0);
			                            } else {
			                                image.setRGB(j, i, color1);
			                            }
			                        }

			                    }
			                }
			                
			                try {
								ImageIO.write( image, "bmp", new File( args[2]) );
								
								System.out.println( "Rings image created successfully");
							 }
							catch (IOException e){
								System.out.println( "The image cannot be stored" );
							 }
	}
	
	
	static int int2RGB( int red, int green, int blue){
		// 	Make sure that color intensities are in 0..255 range
		red = red & 0x000000FF;
		green = green & 0x000000FF;
		blue = blue & 0x000000FF;

		// Assemble packed RGB using bit shift operations
		return (red << 16) + (green << 8) + blue;
	}
	
	private static void drawCircle(BufferedImage image, int centerX, int centerY, int radius,int color) {
        for(int i = centerY-radius; i<centerY+radius; i++) {	
        	 for(int j = centerX-radius; j<centerX+radius; j++) {	
                if (i>=0 && i < image.getHeight()  && j>=0 && j< image.getWidth()) {
                    if (Math.sqrt( ( (centerX -j) * (centerX - j) + (centerY - i) * (centerY - i) ))  <= radius) {
                        image.setRGB(j, i, color);
                    }
                }
            }
        }
    }
	
    private static void drawConcentricCircle(BufferedImage image,int centerX,int centerY,int radius, int ringWidth,int primaryColor,int secondaryColor) {



for(int i = centerY-radius; i<centerY+radius; i++) {	
	 for(int j = centerX-radius; j<centerX+radius; j++) {	
       if (i>=0 && i < image.getHeight()  && j>=0 && j< image.getWidth()) {
           //if (Math.sqrt( ( (centerX -j) * (centerX - j) + (centerY - i) * (centerY - i) ))  <= radius) {
            int ringIndex = ((int) Math.sqrt( ( (centerX -j) * (centerX - j) + (centerY - i) * (centerY - i) )) / ringWidth);

            if (ringIndex % 2 == 0) {
                image.setRGB(j, i, secondaryColor);
            }else {
                image.setRGB(j, i, primaryColor);
            }
       // }
    }
}
}
}
}
