package zadanie1;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Zadanie1cb {

	public static void main(String[] args)	 {
		
		args = new String[4];
		args[0] = "500";
		args[1] =	"500";
		args[2] ="plik1cb";
		
		int x_res = Integer.parseInt( args[0].trim() );
		int y_res = Integer.parseInt( args[1].trim() );

		BufferedImage image = new BufferedImage( x_res, y_res, BufferedImage.TYPE_INT_RGB);;
		int color0 = int2RGB( 0, 255, 0 ); 
		int color1 = int2RGB( 255, 0, 0 ); 
		
		int width = 40;
		int w = width;
		boolean descending = true;
		
		for (int i = 0; i < y_res; i++) 
		{
			int whiteInRow = w;
			int blackInRow = (width - w + 1);
			for (int j = 0; j < x_res; j++)  
			{ 	
				if(whiteInRow > -1)
				{
					image.setRGB( j, i, color0 );
					whiteInRow--;
				}					
				else if(blackInRow > -(width - w -1))
				{
					image.setRGB( j, i, color1 );
					blackInRow--;
				}
				else
				{
					whiteInRow = 2*w;
					blackInRow = width - w + 1;
				}
								
			}
				if(w == -1)
					descending = false;
				if(w == width)
					descending = true;
				if(descending)
					w--;
				else
					w++;			
		}
		
		try {
			ImageIO.write( image, "bmp", new File( args[2]) );
			
			System.out.println( "Chessboard image created successfully");
		 }
		catch (IOException e){
			System.out.println( "The image cannot be stored" );
		 }
	}

	static int int2RGB( int red, int green, int blue)
	{
	// Make sure that color intensities are in 0..255 range
	red = red & 0x000000FF;
	green = green & 0x000000FF;
	blue = blue & 0x000000FF;

	// Assemble packed RGB using bit shift operations
	return (red << 16) + (green << 8) + blue;
	}
}
