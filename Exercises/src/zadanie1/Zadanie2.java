package zadanie1;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Zadanie2 {

	public static void main(String[] args)	 {
		

	String plik2in = "plik2in";
	
	int color0 = int2RGB( 0, 0, 0 );
	int color1 = int2RGB( 255, 255, 255 );
	
	//BufferedImage image = new BufferedImage( x_res, y_res, BufferedImage.TYPE_INT_RGB);

	BufferedImage image;
	try {
		image = ImageIO.read(new File(plik2in));
	
	BufferedImage patternBitmap = generattePatern(image.getWidth(), image.getHeight(), color0, color1);
	int x_res = image.getWidth();
	int y_res = image.getHeight();
	 int maskColor;
     int imageColor;
     
	        for (int i = 0; i < y_res; i++) {
				for (int j = 0; j < x_res; j++) {
					maskColor = patternBitmap.getRGB(j, i);
			        imageColor = image.getRGB(j, i);
			        image.setRGB(j, i, maskColor & imageColor);
			        
				}
			}
	        
	 
	        
	        
	   
				ImageIO.write( image, "bmp", new File( "plik2") );
				
				System.out.println( "Net image created successfully");
	}
			catch (IOException e){
				System.out.println( "The image cannot be stored" );
			 }
	}
	
	 static int int2RGB( int red, int green, int blue)
	{
	// Make sure that color intensities are in 0..255 range
	red = red & 0x000000FF;
	green = green & 0x000000FF;
	blue = blue & 0x000000FF;

	// Assemble packed RGB using bit shift operations
	return (red << 16) + (green << 8) + blue;
	}
	 
	public static BufferedImage generattePatern(int a, int b, int c, int d) {
		
		BufferedImage image = new BufferedImage( a, b, BufferedImage.TYPE_INT_RGB);

		int width = 5;
		int distanceX = 20;
		int distanceY = 20;
		
		boolean startX = false;
		boolean startY = false;
		int counterX = 0;
		int counterY = 0;
		
		for (int i = 0; i < b; i++) {
			for (int j = 0; j < a; j++) {
				image.setRGB( i, j, c );
			}
		}
		
		for (int i = 0; i < b; i++) {
			
			if(i%distanceX == 0) {
					counterX = width;
					startX = true;
				}
			
			counterX--;
			if(counterX == 0)  startX = false;
				
			for (int j = 0; j < a; j++) {
				if(j%distanceY == 0) {
					counterY = width;
					startY = true;
				}
				
				if(startX) {
					image.setRGB( i, j, d );
				}
				
				if(startY) {
					image.setRGB( i, j, d );
				}
				
				
				counterY--;
				
				if(counterY == 0)  startY = false;
			}
		}
		
		return image;
	}
}
